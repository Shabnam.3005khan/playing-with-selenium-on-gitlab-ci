const test = require('tape');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome')

const options  = new chrome.Options();
options.addArguments('headless');
options.addArguments('disable-gpu');
options.addArguments('disable-extensions');
options.addArguments('no-sandbox');
const capabilities = webdriver.Capabilities.chrome();
const driver = new webdriver.Builder()
  .forBrowser('chrome')
  .setChromeOptions(options)
  .withCapabilities(capabilities)
  .build();

test('Some selenium test', (t) => {
  t.plan(1);

  driver.get('http://google.com/')
    .then(() => {
      const searchInput = driver.findElement(webdriver.By.name('q'))
      searchInput.sendKeys('webdriver')
      searchInput.sendKeys(webdriver.Key.ENTER);

      return driver.wait(webdriver.until.titleIs('webdriver - Google Search'), 1000);
    })
    .then(() => {
      t.pass('Successfully ran through selenium tasks');
    })
    .finally(() => {
      driver.quit();
    });
});
